package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type Post struct {
	Title   string   `bson:"title,omitempty"`
	Body    string   `bson:"body,omitempty"`
}

func main() {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb+srv://indra:indra18111998@cluster0.qibtg.mongodb.net/golangmongo?retryWrites=true&w=majority"))
	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(ctx)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}
	databases, err := client.ListDatabaseNames(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(databases)

	collection := client.Database("golangmongo").Collection("sample")

	docs := []interface{}{
            bson.D{{"title" , "World"},{"body" , "Hello World"}},
            bson.D{{"title" , "Mars"},{"body" , "Hello Mars"}},
            bson.D{{"title" , "Pluto" }, {"body" , "Hello Pluto"}},
        }

    res, insertErr := collection.InsertMany(ctx, docs)
    if insertErr != nil {
            log.Fatal(insertErr)
    }
      fmt.Println(res);

	cur, currErr := collection.Find(ctx, bson.D{})

    if currErr != nil { panic(currErr) }
    defer cur.Close(ctx)

    var posts []Post
    if err = cur.All(ctx, &posts); err != nil {
          panic(err)
    }
    fmt.Println(posts)
}